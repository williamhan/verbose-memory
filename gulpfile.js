const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.styles([
    
    'bootstrap.css',
    'animate.min.css',
    'cm-overlay.css',
    'imagehover.css',
    'style.css',
    'smartforms/smart-forms.css',
    'smartforms/smart-addons.css'
])
        .scripts(['basicjs/jquery-1.9.1.min.js',
            'basicjs/jquery.form.min.js',
            'basicjs/jquery.validatebill.min.js',
            'basicjs/additional-methods.min.js',
            'basicjs/bootstrap.min.js',
            'basicjs/custom.js',
            'basicjs/jquery.formShowHide.min.js',
            'basicjs/jquery.maskedinput.js',
            'basicjs/jquery.maskMoney.js',
            'basicjs/jquery.placeholder.min.js',
            'basicjs/jquery-cloneya-bill.js',
            'basicjs/jquery-ui.min.js',
            'basicjs/jquery-ui-custom.min.js',
            'basicjs/jquery-ui-touch-punch.min.js',
            'basicjs/smartforms-modal.min.js',
            'smartforms/steps.bill.js'
        ], 'public/js/basic.js')
        .scripts(['smartforms/smart-form.js'
        ], 'public/js/basictwo.js')
        .scripts(['basicjs/jquery-1.9.1.min.js',
            'basicjs/jquery.form.min.js',
            'basicjs/jquery.validatebill.min.js',
            'basicjs/additional-methods.min.js',
            'basicjs/bootstrap.min.js',
            'basicjs/custom.js',
            'basicjs/jquery.formShowHide.min.js',
            'basicjs/jquery.maskedinput.js',
            'basicjs/jquery.maskMoney.js',
            'basicjs/jquery.placeholder.min.js',
            'basicjs/jquery-cloneya-bill.js',
            'basicjs/jquery-ui.min.js',
            'basicjs/jquery-ui-custom.min.js',
            'basicjs/jquery-ui-touch-punch.min.js',
            'basicjs/smartforms-modal.min.js',
            'smartforms/steps.bill.js'
        ], 'public/js/formtwo/basic.js')
        .scripts(['smartforms/smart-form-two.js'
        ], 'public/js/formtwo/basictwo.js')
        .scripts(['appcdlnew.js',
            'bootstrapcdlnew.js',
            'easing.js',
            'jquery.cm-overlay.js',
            'jquery.flexisel.js',
            'jquery.mobile.custom.min.js',
            'jquery.tools.min.js',
            'move-top.js',
            'wow.min.js'
        ], 'public/js/basicthree.js')
        .version(['css/all.css', 'js/basic.js', 'js/basictwo.js'])
        .copy('resources/assets/fonts', 'public/fonts')
        .copy('resources/assets/images', 'public/images')
        .copy('resources/assets/js/signature', 'public/js/signature')
        .copy('resources/assets/js/cdlnew', 'public/js/cdlnew')
        mix.browserSync({
    browser: "google chrome",
        proxy: 'http://laracdl.dev/test',
    port: 8000
})


      ;;
});

