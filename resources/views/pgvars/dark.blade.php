@include('cdltestpage.phpHeader')
<!DOCTYPE html>
<html>
@include('cdltestpage.htmlhead')
<body>
<!-- banner -->
<div class="bulma-iso">
	@include('pgvars.dark.banner')
</div>	
<!-- //banner -->
<!-- banner-bottom -->
<div class="bulma-iso">
	@include('cdltestpage.bannerbottom')
</div>
<!-- //banner-bottom -->
<!-- banner-bottom1 -->
	<div class="banner-bottom1">
		<div class="col-md-6 agile_banner_bottom1_left">
			{{-- @include('cdltestpage.testsection') --}}
			@include('cdltestpage.formsection')
		</div>
		<div class="col-md-6 agile_banner_bottom1_right">
			@include('cdltestpage.formsectiontwo')
		</div>
		<div class="clearfix"></div>
	</div>
<!-- //banner-bottom1 -->
<!-- testimonials -->
	{{-- @include('cdltestpage.testimonials') --}}
<!-- //testimonials -->
<!-- newsletter -->
	{{-- @include('cdltestpage.newsletter') --}}
<!-- //newsletter -->
<!-- footer -->
<div class="bulma-iso">
	@include('cdltestpage.footer')
</div>
<!-- //footer -->
<!-- for bootstrap working -->
	<script src="js/cdlnew/bootstrapcdlnew.js"></script>
<!-- //for bootstrap working -->
<!-- here starts scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->
</body>
</html>