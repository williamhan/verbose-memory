@if (!empty($pgvar))
    <section class="hero is-small is-{{ $pgvar }} is-bold">
@else
    <section class="hero is-small is-dark is-bold">
@endif
{{-- <section class="hero is-small is-dark is-bold"> --}}
	<div class="hero-foot">
    <nav class="tabs is-boxed is-fullwidth">
  <div class="container">
        <ul>
          <li><a>Lorem</a></li>
          <li><a>Ipsum</a></li>
          <li><a>Lorem</a></li>
          <li><a>Ipsum</a></li>
          <li><a>Lorem</a></li>
          <li><a>Ipsum</a></li>
        </ul>
      </div>
    </nav>
  </div>
  </section>
	{{-- <div class="hero-foot"> --}}
{{-- <footer class="footer">
  <div class="container">
    <div class="content has-text-centered">
      <p>
        <strong>Bulma</strong> by <a href="http://jgthms.com">Jeremy Thomas</a>. The source code is licensed
        <a href="http://opensource.org/licenses/mit-license.php">MIT</a>. The website content
        is licensed <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">CC ANS 4.0</a>.
      </p>
      <p>
        <a class="icon" href="https://github.com/jgthms/bulma">
          <i class="fa fa-github"></i>
        </a>
      </p>
    </div>
  </div>
</footer> --}}
{{-- </div>
</section> --}}



{{-- <div class="footer">
		<div class="container">
			<div class="col-md-4 w3l_footer_grid">
				<h2><a href="index.html"><span>T</span>eaching</a></h2>
				<p>Vestibulum sed convallis massa, eu aliquet massa. Suspendisse 
					lacinia rutrum tincidunt. Integer id erat porta, convallis tortor a, 
					ullamcorper magna.</p>
			</div>
			<div class="col-md-3 w3l_footer_grid">
				<h3>Address</h3>
				<ul class="w3_address">
					<li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>1234k Avenue, 4th block, <span>New York City.</span></li>
					<li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i><a href="mailto:info@example.com">info@example.com</a></li>
					<li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>+1234 567 567</li>
				</ul>
			</div>
			<div class="col-md-2 w3l_footer_grid">
				<h3>Navigation</h3>
				<ul class="agileinfo_footer_grid_nav">
					<li><a href="services.html"><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>Services</a></li>
					<li><a href="portfolio.html"><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>Portfolio</a></li>
					<li><a href="short-codes.html"><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>Short Codes</a></li>
					<li><a href="mail.html"><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>Mail Us</a></li>
				</ul>
			</div>
			<div class="col-md-3 w3l_footer_grid">
				<h3>Social Media</h3>
				<ul class="agileinfo_social_icons1 agileinfo_social">
					<li><a href="#" class="facebook"></a></li>
					<li><a href="#" class="twitter"></a></li>
					<li><a href="#" class="google"></a></li>
					<li><a href="#" class="pinterest"></a></li>
				</ul>
			</div>
			<div class="clearfix"> </div>
			<div class="w3agile_footer_copy">
				<p>© 2017 FreeCdlTest. All rights reserved | Design by <a href="#">Billiam</a></p>
			</div>
		</div>
	</div> --}}