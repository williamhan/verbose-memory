@include('cdltestpage.phpHeader')
<!DOCTYPE html>
<html>
@include('cdltestpage.htmlhead')
<body>
<!-- banner -->
	@include('cdltestpage.banner')
<!-- //banner -->
<!-- banner-bottom -->
	{{-- @include('cdltestpage.bannerbottom') --}}
<!-- //banner-bottom -->
<!-- banner-bottom1 -->
	<div class="banner-bottom1">
		<div class="col-md-6 agile_banner_bottom1_left bulma-iso">
			@include('cdltestpage.testsection')
		</div>
		<div class="col-md-6 agile_banner_bottom1_right">
			@include('cdltestpage.formsection')
		</div>
		<div class="clearfix"> </div>
	</div>
<!-- //banner-bottom1 -->
<!-- testimonials -->
	{{-- @include('cdltestpage.testimonials') --}}
<!-- //testimonials -->
<!-- newsletter -->
	{{-- @include('cdltestpage.newsletter') --}}
<!-- //newsletter -->
<!-- footer -->
@include('cdltestpage.footer')
<!-- //footer -->
<!-- for bootstrap working -->
	<script src="js/cdlnew/bootstrapcdlnew.js"></script>
<!-- //for bootstrap working -->
<!-- here starts scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->
</body>
</html>