@include('cdltestpage.phpHeader')
<!DOCTYPE html>
<html>
@include('cdltestpage.htmlhead')
<body>
<!-- banner -->
<div class="bulma-iso">
	@include('cdltestpage.banner')
</div>	
<!-- //banner -->
<!-- banner-bottom -->
<div class="bulma-iso">
	@include('cdltestpage.bannerbottom')
</div>
<!-- //banner-bottom -->
<!-- banner-bottom1 -->
	<div class="bulma-iso">

@if (!empty($pgvar))
    <section class="hero is-{{ $pgvar }} is-bold">
@else
    <section class="hero is-dark">
@endif

		{{-- <section class="hero is-success is-fullheight"> --}}
  <div class="hero-body">
    <div class="container">
      <div class="typeform-widget" data-url="https://crengland.typeform.com/to/uGFznv" data-transparency="100" data-hide-headers=true data-hide-footer=true style="width: 100%; height: 500px;" > </div> <script> (function() { var qs,js,q,s,d=document, gi=d.getElementById, ce=d.createElement, gt=d.getElementsByTagName, id="typef_orm", b="https://embed.typeform.com/"; if(!gi.call(d,id)) { js=ce.call(d,"script"); js.id=id; js.src=b+"embed.js"; q=gt.call(d,"script")[0]; q.parentNode.insertBefore(js,q) } })() </script> <div style="font-family: Sans-Serif;font-size: 12px;color: #999;opacity: 0.5; padding-top: 5px;" >  </div>
    </div>
  </div>
</section>

		{{-- <div class="col-md-6 agile_banner_bottom1_left">
			@include('cdltestpage.formsection')
		</div>
		<div class="col-md-6 agile_banner_bottom1_right">
			@include('cdltestpage.formsectiontwo')
		</div>
		<div class="clearfix"></div> --}}
	</div>
<!-- //banner-bottom1 -->
<!-- testimonials -->
	{{-- @include('cdltestpage.testimonials') --}}
<!-- //testimonials -->
<!-- newsletter -->
	{{-- @include('cdltestpage.newsletter') --}}
<!-- //newsletter -->
<!-- footer -->
<div class="bulma-iso">

<figure class="image">
		<img src="images/new-banner-cropped-min.jpg">
	</figure>

	@include('cdltestpage.footer')
</div>
<!-- //footer -->
<!-- for bootstrap working -->
	<script src="js/cdlnew/bootstrapcdlnew.js"></script>
<!-- //for bootstrap working -->
<!-- here starts scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->
</body>
</html>