<head>
<title>FreeCdlTest | Home</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Teaching Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->

<link href="css/all.css" rel="stylesheet" type="text/css" media="all" />

<link rel="stylesheet" href="css/bulma-iso.css" type="text/css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<script type="text/javascript" src="js/basic.js"></script>
<script type="text/javascript" src="js/basictwo.js"></script>


<script type="text/javascript" src="js/formtwo/basic.js"></script>
<script type="text/javascript" src="js/formtwo/basictwo.js"></script>



<!-- //js -->

<!—- //JS Files For Mouse Driven Signature —->
<script type="text/javascript" src="js/signature/excanvas.js"></script>
<script type="text/javascript" src="js/signature/jSignature.min.noconflict.js"></script>
<!--[if lt IE 9]>
<script type="text/javascript" src="js/signature/flashcanvas.js"></script>
<![endif]-->

<link href='//fonts.googleapis.com/css?family=Capriola' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/cdlnew/move-top.js"></script>
<script type="text/javascript" src="js/cdlnew/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
<!-- animation-effect -->
<script src="js/cdlnew/wow.min.js"></script>
<script>
 new WOW().init();
</script>
<!-- //animation-effect -->
</head>