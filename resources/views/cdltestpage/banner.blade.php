
@if (!empty($pgvar))
    <section class="hero is-small is-{{ $pgvar }} is-bold">
@else
    <section class="hero is-small is-dark is-bold">
@endif

{{-- <section class="hero is-small is-{{ $pgvar }} is-bold"> --}}
	<div class="hero-head">
		{{-- <div class="container"> --}}
			<nav class="nav">
  			<div class="nav-left">
    			<a class="nav-item">
    				<img src="images/company2.png" alt=" ">
    			</a>
			</div>
			<div class="nav-center">
				<a class="nav-item">
					Home
				</a>
				<a class="nav-item">
					About
				</a>
				<a class="nav-item">
					Contact Us
				</a>
			</div>


			<!-- This "nav-toggle" hamburger menu is only visible on mobile -->
			<!-- You need JavaScript to toggle the "is-active" class on "nav-menu" -->
			
			<span class="nav-toggle">
				<span>Home</span>
				<span>About</span>
				<span>Contact Us</span>
			</span>

			<!-- This "nav-menu" is hidden on mobile -->
			<!-- Add the modifier "is-active" to display it on mobile -->

			<div class="nav-right nav-menu">
				

				<div class="nav-item">
					<div class="field is-grouped">
						<p class="control">
							<a class="button" >
								<span>Test</span>
							</a>
						</p>
						<p class="control">
							<a class="button is-primary">
								<span>Apply</span>
							</a>
						</p>
					</div>
				</div>
			</div>
		</nav>
		{{-- </div> --}}
	</div>
</section>

{{-- 
<div class="banner">
	<div class="header">
		<nav class="nav">
  			<div class="nav-left">
    			<a class="nav-item">
    				<img src="images/company2.png" alt=" ">
    			</a>
			</div>
			<div class="nav-center">
				<a class="nav-item">
					Home
				</a>
				<a class="nav-item">
					About
				</a>
				<a class="nav-item">
					Contact Us
				</a>
			</div>


			<!-- This "nav-toggle" hamburger menu is only visible on mobile -->
			<!-- You need JavaScript to toggle the "is-active" class on "nav-menu" -->
			
			<span class="nav-toggle">
				<span>Home</span>
				<span>About</span>
				<span>Contact Us</span>
			</span>

			<!-- This "nav-menu" is hidden on mobile -->
			<!-- Add the modifier "is-active" to display it on mobile -->

			<div class="nav-right nav-menu">
				

				<div class="nav-item">
					<div class="field is-grouped">
						<p class="control">
							<a class="button" >
								<span>Test</span>
							</a>
						</p>
						<p class="control">
							<a class="button is-primary">
								<span>Apply</span>
							</a>
						</p>
					</div>
				</div>
			</div>
		</nav>
	</div>
 --}}
{{-- END HEADER NAV BAR --}}

{{-- BEGIN HEADER IMAGE AND TEXT --}}

	{{-- <figure class="image">
		<img src="images/new-banner-cropped-min.jpg">
	</figure> --}}



	
</div>